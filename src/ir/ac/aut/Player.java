package ir.ac.aut;

import ir.ac.aut.card.Card;
import ir.ac.aut.card.MonsterCard;
import ir.ac.aut.card.Special;
import ir.ac.aut.card.SpellCard;
import ir.ac.aut.deck.CardDeck;
import ir.ac.aut.deck.SpecialDeck;

/**
 * Created by Sajjad on 4/24/2017.
 */
public class Player {

    CardDeck mainDeck;
    Card[] hand = new Card[5];
    SpecialDeck specialDeck;
    Special nextSpecial;
    int lifePoints;

    public CardDeck getMainDeck() {
        return mainDeck;
    }

    public void setMainDeck(CardDeck mainDeck) {
        this.mainDeck = mainDeck;
    }

    public Card[] getHand() {
        return hand;
    }

    public void setHand(Card[] hand) {
        this.hand = hand;
    }

    public SpecialDeck getSpecialDeck() {
        return specialDeck;
    }

    public void setSpecialDeck(SpecialDeck specialDeck) {
        this.specialDeck = specialDeck;
    }

    public Special getNextSpecial() {
        return nextSpecial;
    }

    public void setNextSpecial(Special nextSpecial) {
        this.nextSpecial = nextSpecial;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public Player(CardDeck mainDeck, SpecialDeck specialDeck, int lifePoints) {
        this.mainDeck = mainDeck;
        this.specialDeck = specialDeck;
        this.lifePoints = lifePoints;
    }

    public Player(CardDeck mainDeck, SpecialDeck specialDeck) {
        this(mainDeck, specialDeck, 5000);
    }

    public boolean draw(int count){
        for(int j=0;j<count;j++) {
            boolean hasNull = false;
            for (int i = 0; i < hand.length; i++) {
                if (hand[i] == null) {
                    if (mainDeck.isEmpty()) return false;
                    hand[i] = mainDeck.deal();
                    hasNull=true;
                    break;
                }
            }
            if(!hasNull && mainDeck.isEmpty())return false;
            else mainDeck.deal();
        }
        return true;
    }

    public void drawSpecialCard() {

        if (nextSpecial == null)
            specialDeck.deal();
    }

    public void nextTurnPrep() {

        if (!this.draw(1))
            lifePoints -= 500;
        this.drawSpecialCard();

    }

    public boolean playCardFromHand(int whichCard, Field myField) {
        if(whichCard>4 || whichCard<0)return false;
        if(hand[whichCard]==null)return false;
        Card theCard = hand[whichCard];
        hand[whichCard]=null;
        boolean didIt = false;
        if(theCard instanceof MonsterCard)didIt =myField.addMonsterCard((MonsterCard)theCard);
        else if (theCard instanceof SpellCard)didIt=myField.addSpellCard((SpellCard)theCard);
        else didIt =false;
        if(didIt == false){
            hand[whichCard]=theCard;
        }
        return didIt;
    }

    public boolean playSpecial(Field myField) {

        return false;
    }

    public void changeLifePoints(int change) {

        change += lifePoints;
    }

    public boolean isDefeated() {

        if (lifePoints <= 0)
            return false;
        else
            return true;
    }

}
