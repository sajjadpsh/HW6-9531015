package ir.ac.aut.card;

import ir.ac.aut.Field;

/**
 * Created by Sajjad on 4/24/2017.
 */
public interface Special {
    public void instantEffect(Field owner, Field enemy);
}
