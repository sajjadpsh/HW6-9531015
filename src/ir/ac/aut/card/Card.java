package ir.ac.aut.card;

/**
 * Created by Sajjad on 4/24/2017.
 */
public abstract class Card {

    String name;
    String description;
    private String type;

    public Card(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {

        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean equals(Card card) {
        if (card.name.equals(getName()) && card.description.equals(getDescription()))
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return name+":"+description;
    }

    public String getType() {
        return type;
    }
}
