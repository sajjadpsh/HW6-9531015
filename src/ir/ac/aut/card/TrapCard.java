package ir.ac.aut.card;

/**
 * Created by Sajjad on 4/24/2017.
 */
public abstract class TrapCard extends Card implements Special {

    public TrapCard(String name, String description) {
        super(name, description);
    }

    public boolean isEqual(TrapCard card) {

        if (name.equals(card.getName()) && description.equals(card.getDescription()))
            return true;
        else
            return false;

    }

}
