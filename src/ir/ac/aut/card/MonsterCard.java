package ir.ac.aut.card;

/**
 * Created by Sajjad on 4/24/2017.
 */
public class MonsterCard extends Card {

    private int power;
    private int basePower;
    private boolean canAttack;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getBasePower() {
        return basePower;
    }

    public void setBasePower(int basePower) {
        this.basePower = basePower;
    }

    public boolean getCanAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public MonsterCard(String name, String description, int power, boolean canAttack) {
        super(name, description);
        this.power = power;
        this.canAttack = canAttack;
    }

    public MonsterCard(String name, String description, int power) {
        this(name, description, power, false);
    }

    @Override
    public String toString() {
        return "MonsterCard{" +
                super.toString() +
                "power=" + power +
                ", canAttack=" + canAttack +
                "} ";
    }


}
