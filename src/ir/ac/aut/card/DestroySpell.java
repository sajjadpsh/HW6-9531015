package ir.ac.aut.card;

import ir.ac.aut.Field;

/**
 * Created by Sajjad on 4/24/2017.
 */
public class DestroySpell {

    TrapCard destroyer = new TrapCard("Destroy Spell", "Destroys the enemy’s first spell card.") {
        @Override
        public void instantEffect(Field owner, Field enemy) {

        }
    };

}
