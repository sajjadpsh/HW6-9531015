package ir.ac.aut.card;

import ir.ac.aut.Field;

/**
 * Created by Sajjad on 4/24/2017.
 */
public abstract class SpellCard extends Card {
    public SpellCard(String name, String description) {
        super(name, description);
    }

    public abstract void turnEffect(Field ownerField, Field enemyField);

    public abstract void destroyedEffect(Field ownerField, Field enemyField);

    public boolean isEqual(SpellCard card) {
        return name.equals(card.getName()) && description.equals(card.getDescription());
    }

}
