package ir.ac.aut.card;

import ir.ac.aut.Field;

/**
 * Created by Sajjad on 4/24/2017.
 */
public class PowerCard extends SpellCard {

    private int power;

    public PowerCard(String name, String description, int power) {
        super(name, description);
        this.power = power;
    }

    public PowerCard(){
        super("","");
    }

//    SpellCard power = new SpellCard("Power Card", "Increases power of monsters by 100 each turn.")

    @Override
    public void turnEffect(Field ownerField, Field enemyField) {

        MonsterCard[] monsterCards = ownerField.getMonsters();
        for (MonsterCard monsterCard : monsterCards) {
            if (monsterCards != null)
                monsterCard.setPower(monsterCard.getPower() + 100);
        }}


    public void destroyedEffect(Field ownerField, Field enemyField) {

        MonsterCard[] monsterCards = ownerField.getMonsters();
        for (MonsterCard monsterCard : monsterCards) {
            if (monsterCards != null)
                monsterCard.setPower(monsterCard.getPower() - 300);
        }
    }
}



