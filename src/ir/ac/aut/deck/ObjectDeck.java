package ir.ac.aut.deck;

import ir.ac.aut.card.BlueEyesWhiteDragon;
import ir.ac.aut.card.DestroySpell;
import ir.ac.aut.card.MonsterCard;

/**
 * Created by Sajjad on 4/24/2017.
 */
public abstract class ObjectDeck {

    private Object[] objects=new Object[7777];

    public ObjectDeck(Object[] objects) {
        this.objects = objects;
    }


    public ObjectDeck(DestroySpell destroySpell, BlueEyesWhiteDragon blueEyesWhiteDragon) {

    }


    public Object deal() {
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] == null) {
                continue;
            } else {
                Object tmp = objects[i];
                objects[i] = null;
                return tmp;
            }
        }
        return null;
    }

    public int size() {
        return objects.length;
    }

    public boolean isEmpty() {
        if (size() == 0)
            return true;
        else
            return false;
    }
}
