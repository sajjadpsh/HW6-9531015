package ir.ac.aut.deck;

import ir.ac.aut.card.BlueEyesWhiteDragon;
import ir.ac.aut.card.DestroySpell;

/**
 * Created by Sajjad on 4/24/2017.
 */
public class SpecialDeck extends ObjectDeck {


    public SpecialDeck(Object[] objects) {
        super(objects);
    }

    public SpecialDeck deal(){return (SpecialDeck) super.deal();}

    public SpecialDeck(DestroySpell destroySpell, BlueEyesWhiteDragon blueEyesWhiteDragon) {
        super(destroySpell, blueEyesWhiteDragon);
    }
}
